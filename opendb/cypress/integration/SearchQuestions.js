import SearchQuestions from "../support/pages/SearchQuestions/SearchQuestions";
import Utils from "../Utils/validations";

const elements = require("../support/pages/SearchQuestions/SearchQuestionsElements").ELEMENTS;

describe("Realizando testes de buscas de questões.", () => {
  beforeEach(() => {
    cy.clearCookies();
    cy.clearLocalStorage();
    SearchQuestions.acessQuestions();
  });

  const question = {
    searchKeys: Cypress.env("SearchKey"),
    searchKeysCategory: Cypress.env("SearchKeyCategory"),
    serachKeysUser: Cypress.env("SearchKeyUser"),
    typeUser: Cypress.env("SelectorUser"),
    typeCategory: Cypress.env("SelectorCategory")
  };

  it("Realizar a busca de questões", () => {
    SearchQuestions.searchQuestion(question);
    Utils.validateItemVisible(elements.resultList)
  })

 
  it("Realizar a busca de questões sem parametro de busca", () => {
    const searchEmptyQuestions = Object.create(question);
    searchEmptyQuestions.searchKeys = "";
    SearchQuestions.searchQuestion(searchEmptyQuestions);
    Utils.validateItemVisible(elements.resultList)

  });

  it("Realizar a busca de questões sem parametro de busca", () => {
    const searchForceErrorQuestions = Object.create(question);
    searchForceErrorQuestions.searchKeys = "ajhsdflkjashdflkjashf";
    SearchQuestions.searchQuestion(searchForceErrorQuestions);
    Utils.validateTextItemVisible(elements.errorAlert, "No questions found.")
  });

  it("Validar Títulos de informações", () => {
    SearchQuestions.searchQuestion(question);
    Utils.validateTextItemVisible(elements.questionID, "ID");
    Utils.validateTextItemVisible(elements.questionCategory, "Category");
    Utils.validateTextItemVisible(elements.questionType, "Type");
    Utils.validateTextItemVisible(elements.questionDifficulty, "Difficulty");
    Utils.validateTextItemVisible(elements.questionStatement, "Question / Statement");
    Utils.validateTextItemVisible(elements.questionAuthor, "Created By");
    Utils.validateItemVisible(elements.questionsPagination)
  });

it("Realizar a busca de questões por categoria", () => {
  SearchQuestions.searchQuestionCategory(question);
  Utils.validateItemVisible(elements.resultList)
})

it("Realizar a busca de questões por categoria sem parametro de busca", () => {
  const searchEmpCategory = Object.create(question);
  searchEmpCategory.searchKeysCategory = "";
  SearchQuestions.searchQuestionCategory(searchEmpCategory);
  Utils.validateItemVisible(elements.resultList)
});

it("Realizar a busca de questões por categoria com sem parametro errado", () => {
  const searchForceErrorQuestionsCategory = Object.create(question);
  searchForceErrorQuestionsCategory.searchKeysCategory = "ajhsdflkjashdflkjashf";
  SearchQuestions.searchQuestionCategory(searchForceErrorQuestionsCategory);
  Utils.validateTextItemVisible(elements.errorAlert, "No questions found.")
});

it("Validar Títulos de informações de busca realizada por categoria", () => {
  SearchQuestions.searchQuestionCategory(question);
  Utils.validateTextItemVisible(elements.questionID, "ID");
  Utils.validateTextItemVisible(elements.questionCategory, "Category");
  Utils.validateTextItemVisible(elements.questionType, "Type");
  Utils.validateTextItemVisible(elements.questionDifficulty, "Difficulty");
  Utils.validateTextItemVisible(elements.questionStatement, "Question / Statement");
  Utils.validateTextItemVisible(elements.questionAuthor, "Created By");
  Utils.validateItemVisible(elements.questionsPagination)
});

it("Realizar a busca de questões por usuário", () => {
  SearchQuestions.searchQuestionUser(question);
  Utils.validateItemVisible(elements.resultList)
})

it("Realizar a busca de questões por usuário sem parametro de busca", () => {
  const searchEmpUser = Object.create(question);
  searchEmpUser.serachKeysUser = "";
  SearchQuestions.searchQuestionUser(searchEmpUser);
  Utils.validateTextItemVisible(elements.errorAlert, "No User Found!")
});

it("Realizar a busca de questões por usuário com parametro errado", () => {
  const searchEmpUser = Object.create(question);
  searchEmpUser.serachKeysUser = "asdfasdfasdfasd";
  SearchQuestions.searchQuestionUser(searchEmpUser);
  Utils.validateTextItemVisible(elements.errorAlert, "No User Found!")
});

it("Validar Títulos de informações de busca realizada por usuário", () => {
  SearchQuestions.searchQuestionCategory(question);
  Utils.validateTextItemVisible(elements.questionID, "ID");
  Utils.validateTextItemVisible(elements.questionCategory, "Category");
  Utils.validateTextItemVisible(elements.questionType, "Type");
  Utils.validateTextItemVisible(elements.questionDifficulty, "Difficulty");
  Utils.validateTextItemVisible(elements.questionStatement, "Question / Statement");
  Utils.validateTextItemVisible(elements.questionAuthor, "Created By");
  Utils.validateItemVisible(elements.questionsPagination)
});

});


const elements = require("./SearchQuestionsElements").ELEMENTS;

class SearchQuestions {
  acessQuestions() {
    cy.visit(`${Cypress.env("baseUrl", "browse.php")}`, {
      onBeforeLoad(win) {
        delete win.fetch;
      },
    });
  }

  searchQuestion(obj){
    if (obj.searchKeys !== "") {
      cy.get(elements.textinputQuestion).type(obj.searchKeys);
    }
    cy.get(elements.buttonSearch).first().click();
  }


  searchQuestionCategory(obj){
    cy.get(elements.selecTypeUser).select(obj.typeCategory);
    
    if (obj.searchKeysCategory !== ""){
      cy.get(elements.textinputQuestion).type(obj.searchKeysCategory);
    }
    
    cy.get(elements.buttonSearch).first().click();
  }

  searchQuestionUser(obj){
    cy.get(elements.selecTypeUser).select(obj.typeUser);
    
    if (obj.serachKeysUser !== ""){
      cy.get(elements.textinputQuestion).type(obj.serachKeysUser);
    }
    
    cy.get(elements.buttonSearch).first().click();
  }

}



export default new SearchQuestions();

export const ELEMENTS = {
    textinputQuestion: "#query",
    selecTypeUser: "#type",
    selectType: "select#type.form-control",
    buttonSearch: ".btn",
    selectorTypeQuestion: "#type",
    resultList: "tbody > :nth-child(1) > :nth-child(1)",
    errorAlert: ".alert",
    errorUserAlert: "div.alert.alert-danger",
    questionID: "table.table.table-bordered > thead > tr > th:nth-child(1)",
    questionCategory: "table.table.table-bordered > thead > tr > th:nth-child(2)",
    questionType: "table.table.table-bordered > thead > tr > th:nth-child(3)",
    questionDifficulty: "table.table.table-bordered > thead > tr > th:nth-child(4)",
    questionStatement: "table.table.table-bordered > thead > tr > th:nth-child(5)",
    questionAuthor: "table.table.table-bordered > thead > tr > th:nth-child(6)",
    questionsPagination : "ul.pagination.pagination-lg"

  };